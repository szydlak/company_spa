import React from 'react';
import Container from '@material-ui/core/Container'
import CompanySearch from './Components/CompanySearch'
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div>
      <Container maxWidth="sm">
          <CompanySearch />
        </Container>      
    </div>
  );
}

export default App;
