

import React, { Component, Fragment} from 'react';
import CompanyView from './CompanyView'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField'

const styles = theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  });

class CompanySearch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchValue : "",
            wasClicked : false,    
            company : {
                name : "",
                city : "",
                postCode : "",
                street : "",
                number : ""
            }
        }
    }
    onChange =(e)=> {
        var value = e.target.value;
        this.setState({searchValue : value});
    }

    searchClick =() => {
        this.setState({wasClicked : true});  
       
        if (this.state.searchValue.length>0)
            {
                fetch('http://localhost:60935/api/companies/get/' + this.state.searchValue) // wiem to powinien być adres ip wczytany z konfiga
                .then(response => response.json())
                .then(data => this.setState({ company : data }))
                .catch((error)=> {console.log(error)})        
            }
    }
    render() {
        
        const { classes } = this.props;
        const showCompanyInfo = this.state.wasClicked;
        let companyInfoView;
        if (showCompanyInfo)
        {
            companyInfoView = <Grid item xs={12}>
                                <Paper className={classes.paper}>
                                    <CompanyView company={this.state.company}/>  
                                </Paper>
                            </Grid>
        }
        return (
             <Grid container spacing={3}>
                <Grid item xs={12}>
                <Paper className={classes.paper}>
                    <TextField
                            required
                            id="standard"                        
                            defaultValue= {this.state.searchValue}
                            onChange={this.onChange}
                            margin="normal"
                    />
                </Paper>
                <Paper className={classes.paper}>
                        <Button variant="outlined" onClick={this.searchClick}>Find</Button>
                    </Paper>                     
                </Grid>
                <Grid item xs={6}>
                                       
                </Grid>
                {companyInfoView}                
            </Grid>  
         
        
        
        );
    }
}

CompanySearch.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles)(CompanySearch);