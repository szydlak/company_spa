import React from 'react';
import TextField from '@material-ui/core/TextField'
export default class CompanyView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            company: props.company
        }
    }
    componentWillReceiveProps = (props) => {
        
        this.setState({
            company: props.company
        })
    }
    render () {       
        console.log(this.state.company.name);
        return(
            <div>
                <h3>Company info</h3>
                <TextField
                    value = {this.state.company.name}                  
                    margin="normal"
                    style = {{width: 300}}                    
                />
                <TextField
                    value = {this.state.company.city}                  
                    margin="normal"
                    style = {{width: 150, marginLeft: 10}}                    
                />
                <TextField
                    value = {this.state.company.postCode}                  
                    margin="normal"
                    style = {{width: 150, marginLeft: 10}}                    
                />
                <TextField
                    value = {this.state.company.street}                  
                    margin="normal"
                    style = {{width: 150, marginLeft: 10}}                    
                />
                 <TextField
                    value = {this.state.company.number}                  
                    margin="normal"
                    style = {{width: 150, marginLeft: 10}}                    
                />               
            </div>
        );
    }
}